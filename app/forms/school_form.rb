class SchoolForm
  include ActiveModel::Model

  attr_accessor :name, :street, :state, :zip_code, :country
  attr_reader :school, :address

  validates :name, presence: true
  validates :street, presence: true
  validates :zip_code, presence: true, length: { is: 5 }
  validates :country, presence: true
  validates :state, presence: true

  def save
    if valid?
      process!
      true
    else
      false
    end
  end

  def process!
    create_school
    create_address
  end

  def create_school
    @school ||= School.create!(name: name)
  end

  def create_address
    @address ||= Address.create!(street: street, state: state, zip_code: zip_code, country: country, owner: @school)
  end
end
