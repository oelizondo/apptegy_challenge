class OrderForm
  include ActiveModel::Model

  attr_accessor :send_email, :gift_ids, :recipient_ids, :school
  attr_reader :order

  validates :gift_ids, presence: true
  validates :recipient_ids, presence: true
  validates :school, presence: true

  def save
    if valid?
      process!
      true
    else
      false
    end
  end

  def process!
    @order ||= Order.create(
      send_email: send_email,
      gifts: gifts,
      recipients: recipients,
      school: school
    )
  end

  def gifts
    Gift.where(id: gift_ids)
  end

  def recipients
    Recipient.where(id: recipient_ids)
  end
end