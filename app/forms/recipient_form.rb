class RecipientForm
  include ActiveModel::Model

  attr_accessor :first_name, :last_name, :street, :state, :zip_code, :country, :school
  attr_reader :recipient, :address

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :street, presence: true
  validates :zip_code, presence: true, length: { is: 5 }
  validates :country, presence: true
  validates :state, presence: true
  validates :school, presence: true

  def save
    if valid?
      process!
      true
    else
      false
    end
  end

  def process!
    create_recipient
    create_address
  end

  def create_recipient
    @recipient ||= Recipient.create!(
      first_name: first_name,
      last_name: last_name,
      school: school
    )
  end

  def create_address
    @address ||= Address.create!(street: street, state: state, zip_code: zip_code, country: country, owner: @recipient)
  end
end