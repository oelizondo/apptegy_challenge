module Auth
  class AuthenticateUser
    attr_reader :success, :data

    def initialize(params)
      @email = params[:email]
      @password = params[:password]
      @success = false
    end

    def authenticate!
      return unless user
      @success = true
      @data = JsonWebToken.encode(user_id: user.id)
      self
    end

    private

    def user
      user = User.find_by(email: @email)
      return user if user && user.authenticate(@password)
      nil
    end
  end
end