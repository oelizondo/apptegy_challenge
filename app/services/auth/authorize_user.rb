module Auth
  class AuthorizeUser
    attr_reader :errors

    def initialize(headers = {})
      @headers = headers
      @errors = []
    end

    def authorize
      user
    end

    private

    def user
      @user ||= User.find(decoded_token[:user_id]) if decoded_token
      @user || @errors << {:token => 'Invalid Token'} && nil
    end

    def decoded_token
      @token ||= JsonWebToken.decode(parse_headers)
    end

    def parse_headers
      if @headers['Authorization'].present?
        return @headers['Authorization'].split(' ').last
      else
        @errors << {:token => 'Missing Token'}
      end
      nil
    end
  end
end