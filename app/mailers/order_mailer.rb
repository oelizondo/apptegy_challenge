class OrderMailer < ApplicationMailer
  def order_shipped
    @order = params[:order]
    @order.recipients.each
    mail(to: @order.recipients, subject: "Your gift(s) from #{@order.school} are on the way!")
  end
end
