class SchoolSerializer < ActiveModel::Serializer
  attributes :id, :name
  has_one :address, as: :owner
end
