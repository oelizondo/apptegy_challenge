class OrderSerializer < ActiveModel::Serializer
  attributes :id, :status, :gifts, :recipients

  belongs_to :school
  has_many :gifts
  has_many :recipients
end