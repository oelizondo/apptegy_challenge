class RecipientSerializer < ActiveModel::Serializer
  attributes :id, :first_name, :last_name

  has_one :address, as: :owner
end