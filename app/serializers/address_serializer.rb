class AddressSerializer < ActiveModel::Serializer
  attributes :id, :street, :zip_code, :state
end
