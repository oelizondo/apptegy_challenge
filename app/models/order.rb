class Order < ApplicationRecord
  STATUSES = %w(order_received order_processing order_shipped order_cancelled)
  has_many :shipments
  has_many :recipients, through: :shipments
  has_many :gift_orders
  has_many :gifts, through: :gift_orders
  belongs_to :school

  validates :status, presence: true, inclusion: { in: STATUSES }
  validates :school, presence: true
  validate :amount_of_recipients
  validate :amount_of_gifts_shipped

  def update_order(params)
    if status != "order_shipped"
      update(params)
    else
      errors.add(:base, "The order has already shipped.")
      false
    end
  end

  def ship!
    update_attribute(:status, "order_shipped")
    send_shipment_email if send_email
    true
  end
  
  def cancel!
    if status != "order_shipped"
      update_attribute(:status, "order_cancelled")
    else
      errors.add(:base, "The order has already shipped.")
      false
    end
  end

  def received!
    update_attribute(:status, "order_received")
  end

  private

  def send_shipment_email
    OrderMailer.with(order: self).order_shipped.deliver_later
  end

  def amount_of_recipients
    if recipients.length > 20
      errors.add(:base, "There can be no more than 20 recipients.")
    end
  end

  def amount_of_gifts_shipped
    if amount_of_gifts_today > 60
      errors.add(:base, "There can only be 60 gifts shipped per day.")
    end
  end

  def amount_of_gifts_today
    GiftOrder.where(
      order_id: id,
      created_at: Time.now.beginning_of_day..Time.now.end_of_day
    ).count
  end
end