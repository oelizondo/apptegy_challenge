class Address < ApplicationRecord
  belongs_to :owner, polymorphic: true

  validates :street, presence: true
  validates :zip_code, presence: true, length: { is: 5 }
  validates :country, presence: true
  validates :state, presence: true
end