class User < ApplicationRecord
  has_secure_password

  validates :password, length: { minimum: 8 }, presence: true
  validates :password_confirmation, length: { minimum: 8 }, presence: true
  validates :email, presence: true, uniqueness: true
end