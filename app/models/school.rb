class School < ApplicationRecord
  has_one :address, as: :owner
  has_many :recipients
  has_many :orders

  validates :name, presence: true
end
