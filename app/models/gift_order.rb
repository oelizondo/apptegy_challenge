class GiftOrder < ApplicationRecord
  belongs_to :gift
  belongs_to :order
end