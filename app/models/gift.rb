class Gift < ApplicationRecord
  KINDS = %w(mug t_shirt hoodie sticker)
  has_many :gift_orders
  has_many :orders, through: :gift_orders

  validates :kind, presence: true, inclusion: { in: KINDS }
end