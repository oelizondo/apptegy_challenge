class Recipient < ApplicationRecord
  has_one :address, as: :owner
  belongs_to :school
  has_many :shipments
  has_many :orders, through: :shipments

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :school, presence: true
end