class ApplicationController < ActionController::API
  before_action :authorize_request, except: :root
  attr_reader :current_user

  def root
    render json: { hello: "world" }, status: 200
  end

  private

  def authorize_request
    @current_user = Auth::AuthorizeUser.new(request.headers).authorize
    render json: { error: 'Not Authorized' }, status: 401 unless @current_user
  end
end
