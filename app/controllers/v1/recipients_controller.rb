class V1::RecipientsController < ApplicationController
  before_action :find_school, only: [:index, :create]
  before_action :set_recipient, only: [:update, :destroy]

  def index
    render json: @school.recipients, status: 200
  end

  def create
    @recipient_form = RecipientForm.new(recipient_form_params)

    if @recipient_form.save
      render json: @recipient_form.recipient, status: 201
    else
      render json: @recipient_form.errors.messages, status: 500
    end
  end

  def update
    if @recipient.update(recipient_params)
      render json: @recipient, status: 202
    else
      render json: @recipient.errors.messages, status: 500 
    end
  end

  def destroy
    @recipient.delete
    head :ok
  end

  private

  def recipient_form_params
    recipient_params
      .merge(address_attributes)
      .merge(school: @school)
  end

  def recipient_params
    params.require(:recipient).permit(:first_name, :last_name)
  end

  def address_attributes
    params.require(:address).permit(:street, :zip_code, :country, :state)
  end

  def find_school
    @school = School.find(params[:school_id])
  end

  def set_recipient
    @recipient = Recipient.find(params[:id])
  end
end