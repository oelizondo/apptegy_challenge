class V1::OrdersController < ApplicationController
  before_action :find_school, only: [:index, :create, :cancel, :ship]
  before_action :set_order, only: [:update, :cancel, :ship, :received]
  
  def index
    render json: @school.orders
  end

  def show
    render json: Order.find(params[:id])
  end
  
  def create
    @order_form = OrderForm.new(order_form_params)

    if @order_form.save
      render json: @order_form.order, status: 201
    else
      render json: @order_form.errors.messages, status: 400
    end
  end

  def update
    if @order.update_order(order_params)
      render json: @order, status: 204
    else
      render json: @order.errors.messages, status: 400
    end
  end

  def cancel
    if @order.cancel!
      render json: @order, status: 200
    else
      render json: @order.errors.messages, status: 500
    end
  end

  def ship
    if @order.ship!
      render json: @order, status: 200
    else
      render json: @order.errors.messages, status: 500
    end
  end

  def received
    if @order.received!
      render json: @order, status: 200
    else
      render json: @order.errors.messages, status: 500
    end
  end

  private

  def order_form_params
    order_params.merge(school: @school)
  end

  def order_params
    params.require(:order).permit(:send_email, gift_ids: [], recipient_ids: [])
  end

  def find_school
    @school = School.find(params[:school_id])
  end

  def set_order
    @order = Order.find(params[:id])
  end
end