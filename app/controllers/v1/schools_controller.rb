class V1::SchoolsController < ApplicationController
  def index
    @schools = School.all
    render json: @schools, status: 200
  end

  def show
    @school = School.find(params[:id])
    render json: @school, status: 200
  end
  
  def create
    @school_form = SchoolForm.new(school_form_params)

    if @school_form.save
      render json: @school_form.school, status: 201
    else
      render json: @school.errors.messages, status: 500
    end
  end

  def update
    @school = School.find(params[:id])

    if @school.update(school_params)
      render json: @school, status: 202
    else
      render json: @school.errors.messages, status: 500 
    end
  end

  def destroy
    @school = School.find(params[:id]) 
    @school.delete
    head :ok
  end

  private

  def school_form_params
    school_params.merge(address_params)
  end

  def school_params
    params.require(:school).permit(:name)
  end

  def address_params
    params.require(:address).permit(:street, :zip_code, :country, :state)
  end
end
