class V1::Users::AuthenticationController < ApplicationController
  skip_before_action :authorize_request

  def authenticate
    result = authenticate_user

    if result != nil && result.success
      render json: { auth_token: result.data }, status: 200
    else
      render json: { error: 'Invalid Credentials' }, status: :unauthorized
    end
  end

  private

  def user_params
    params.require(:user_auth).permit(:email, :password)
  end

  def authenticate_user
    Auth::AuthenticateUser.new(user_params).authenticate!
  end
end