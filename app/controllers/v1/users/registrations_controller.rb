class V1::Users::RegistrationsController < ApplicationController
  skip_before_action :authorize_request, only: :create

  def create
    user = User.create(registration_params)
    if user.save
      result = authenticate
      render json: { auth_token: result.data }
    else
      render json: { error: 'Something went wrong' }, status: 500
    end
  end

  private

  def registration_params
    params.require(:sign_up).permit(:email, :password, :password_confirmation)
  end

  def authenticate
    Auth::AuthenticateUser.new(registration_params).authenticate!
  end
end
