require 'rails_helper'

describe School do
  context "validations" do
    it { is_expected.to validate_presence_of(:name) }
  end

  context "associations" do
    it { is_expected.to have_one(:address) }
    it { is_expected.to have_many(:recipients) }
    it { is_expected.to have_many(:orders) }
  end
end