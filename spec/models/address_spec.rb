require 'rails_helper'

describe Address do
  context "validations" do
    it { is_expected.to validate_presence_of(:street) }
    it { is_expected.to validate_presence_of(:zip_code) }
    it { is_expected.to validate_presence_of(:country) }
    it { is_expected.to validate_presence_of(:state) }

    it { is_expected.to validate_length_of(:zip_code).is_equal_to(5) }
  end
  
  context "associations" do
    it { is_expected.to belong_to(:owner) }    
  end
end
