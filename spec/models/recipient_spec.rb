require 'rails_helper'

describe Recipient do
  context "validations" do
    it { is_expected.to validate_presence_of(:first_name) }
    it { is_expected.to validate_presence_of(:last_name) }
    it { is_expected.to validate_presence_of(:school) }
  end
  
  context "associations" do
    it { is_expected.to have_one(:address) }
    it { is_expected.to belong_to(:school) }
    it { is_expected.to have_many(:shipments) }
    it { is_expected.to have_many(:orders) }
  end
end
