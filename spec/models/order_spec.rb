require "rails_helper"

describe Order do
  context "validations" do
    it { is_expected.to validate_presence_of(:status)}
  end
  
  context "associations" do
    it { is_expected.to belong_to(:school)}
    it { is_expected.to have_many(:shipments)}
    it { is_expected.to have_many(:recipients)}
    it { is_expected.to have_many(:gift_orders)}
    it { is_expected.to have_many(:gifts)}
  end

  describe "#create" do
    it "should not create an order if there are more than 20 recipients" do
      recipients = create_list(:recipient, 21)
      order = build :order, recipients: recipients

      expect(order.valid?).to be_falsy
    end
  end

  describe "update_order" do
    it "should update the order with new data" do
      order = create :order

      order.update_order(status: "order_cancelled")

      expect(order.status).to eq("order_cancelled")
    end

    it "should not update the order if status is ordered_shipped" do
      order = create :order, status: "order_shipped"

      order.update_order(status: "order_cancelled")
      puts order.errors[:shipment]

      expect(order.status).to eq("order_shipped")
      expect(order.errors[:base].first).to eq("The order has already shipped.")
    end
  end

  describe "#ship!" do
    it "should mark order status as order_shipped" do
      order = create :order, send_email: true

      order.ship!

      expect(order.status).to eq("order_shipped")
    end
  end

  describe "#cancel!" do
    it "should mark order status as order_cancelled" do
      order = create :order

      order.cancel!

      expect(order.status).to eq("order_cancelled")
    end

    it "should not change status if status is order_shipped" do
      order = create :order

      order.ship!
      cancelled = order.cancel!

      expect(cancelled).to be_falsy
      expect(order.errors[:base].first).to eq("The order has already shipped.")
    end
  end
end