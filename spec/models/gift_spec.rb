require 'rails_helper'

describe Gift do
  context "validations" do
    it { is_expected.to validate_presence_of(:kind) }
  end

  context "associations" do
    it { is_expected.to have_many(:gift_orders) }
    it { is_expected.to have_many(:orders) }
  end
end
