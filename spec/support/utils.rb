def create_token
  user = create :user
  JsonWebToken.encode(user_id: user.id)
end