FactoryBot.define do
  factory :address do
    street { FFaker::AddressUS.street_name }
    zip_code { FFaker::AddressMX.zip_code }
    country { "US" }
    state { FFaker::AddressUS.state }
    owner factory: :recipient 
  end
end
