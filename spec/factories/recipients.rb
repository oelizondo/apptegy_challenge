FactoryBot.define do
  factory :recipient do
    first_name { FFaker::Name.first_name }
    last_name { FFaker::Name.last_name }
    school
  end
end
