FactoryBot.define do
  factory :gift do
    kind { ["mug", "t_shirt", "hoodie", "sticker"].sample }
  end
end