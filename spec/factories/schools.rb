FactoryBot.define do
  factory :school do
    name { FFaker::Education.school }
  end
end
