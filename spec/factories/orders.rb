FactoryBot.define do
  factory :order do
    status { "order_processing"}
    school factory: :school
  end
end
