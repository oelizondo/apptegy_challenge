require "rails_helper"

describe V1::Users::AuthenticationController do
  describe "POST /v1/users/authenticate" do
    it "creates order" do
      user = create :user

      post :authenticate, params: {
        user_auth: {
          email: user.email,
          password: "12341234"
        }
      }

      token = JSON.parse(response.body)["auth_token"]

      expect(response).to have_http_status(:success)
      expect(token).to_not be_nil
    end
  end
end
