require "rails_helper"

describe V1::Users::RegistrationsController do
  describe "POST /v1/users/sign_up" do
    it "creates a user" do
      post :create, params: {
        sign_up: {
          email: "oscar@gmail.com",
          password: "12341234",
          password_confirmation: "12341234"
        }
      }

      token = JSON.parse(response.body)["auth_token"]

      expect(response).to have_http_status(:success)
      expect(token).to_not be_nil
      expect(User.count).to eq(1)
    end
  end
end
