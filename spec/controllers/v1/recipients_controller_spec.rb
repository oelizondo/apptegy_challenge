require "rails_helper"

describe V1::RecipientsController do
  before do
    request.headers["Authorization"] = "Bearer #{create_token}"
  end

  describe "POST /v1/schools/:id/recipients" do
    it "creates recipient" do
      school = create :school

      post :create, params: {
        school_id: school.id,
        recipient: { 
          first_name: FFaker::Name.first_name,
          last_name: FFaker::Name.last_name,
        },
        address: {
          street: "Street X",
          zip_code: "66230",
          country: "US",
          state: "TX"
        }
      }

      expect(response).to have_http_status(:success)
      expect(Recipient.count).to eq(1)
    end
  end

  describe "PUT /v1/schools/:id/recipients/:id" do
    it "updates a recipient" do
      school = create :school
      recipient = create :recipient, school: school

      patch :update, params: {
        school_id: school.id,
        id: recipient.id,
        recipient: {
          first_name: "NewName",
          last_name: "NewLastName"
        }
      }

      expect(response).to have_http_status(:success)
      expect(Recipient.first.first_name).to eq("NewName")
    end
  end

  describe "DELETE /v1/schools/:id/recipients/:id" do
    it "deletes a school" do
      school = create :school
      recipient = create :recipient, school: school

      delete :destroy, params: { school_id: school.id, id: recipient.id }

      expect(response).to have_http_status(:success)
      expect(Recipient.count).to eq(0)
    end
  end
end
