require "rails_helper"

describe V1::SchoolsController do
  before do
    request.headers["Authorization"] = "Bearer #{create_token}"
  end

  describe "POST /v1/schools" do
    it "creates school" do
      post :create, params: {
        school: { name: "APS" },
        address: {
          street: "Street X",
          zip_code: "66230",
          country: "US",
          state: "TX"
        }
      }

      expect(response).to have_http_status(:success)
      expect(School.count).to eq(1)
    end
  end

  describe "GET /v1/schools/" do
    before do
      create :school
    end
    
    it "gets schools" do
      get :index

      expect(response).to have_http_status(:success)
      expect(School.count).to eq(1)
    end
  end

  describe "PUT /v1/schools/:id" do
    it "updates a school" do
      address = create :address
      school = create :school, address: address

      patch :update, params: {
        id: school.id,
        school: { name: "Updated Name" }
      }

      expect(response).to have_http_status(:success)
      expect(school.reload.name).to eq("Updated Name")
      expect(School.count).to eq(2)
    end
  end

  describe "DELETE /v1/schools/:id" do
    it "updates a school" do
      school = create :school
      delete :destroy, params: { id: school.id }

      expect(response).to have_http_status(:success)
      expect(School.count).to eq(0)
    end
  end
end
