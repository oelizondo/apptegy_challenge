require "rails_helper"

describe V1::OrdersController do
  let(:address) { create :address }
  let(:school) { create :school, address: address }
  let(:gift) { create :gift }
  let(:recipient) { create :recipient }

  before do
    request.headers["Authorization"] = "Bearer #{create_token}"
  end

  describe "POST /v1/schools/:id/orders" do
    it "creates order" do
      post :create, params: {
        school_id: school.id,
        order: {
          send_email: false,
          gift_ids: [gift.id],
          recipient_ids: [recipient.id]
        }
      }

      expect(response).to have_http_status(:success)
      expect(Order.count).to eq(1)
    end
  end

  describe "PUT /v1/schools/:school_id/orders/:id" do
    it "updates an order" do
      order = create :order

      patch :update, params: {
        school_id: school.id,
        id: order.id,
        order: {
          send_email: true
        }
      }

      expect(response).to have_http_status(:success)
      expect(order.reload.send_email).to be_truthy
    end
  end

  describe "GET /v1/schools/:school_id/orders/:id/cancel" do
    it "cancels an order" do
      order = create :order

      get :cancel, params: { school_id: school.id, id: order.id }

      expect(response).to have_http_status(:success)
      expect(order.reload.status).to eq("order_cancelled")
    end
  end

  describe "POST /v1/schools/:school_id/orders/:id/ship" do
    it "ships an order" do
      order = create :order

      post :ship, params: { school_id: school.id, id: order.id }

      expect(response).to have_http_status(:success)
      expect(order.reload.status).to eq("order_shipped")
    end
  end

  describe "POST /v1/schools/:school_id/orders/:id/received" do
    it "marks an order as received" do
      order = create :order

      post :received, params: { school_id: school.id, id: order.id }

      expect(response).to have_http_status(:success)
      expect(order.reload.status).to eq("order_received")
    end
  end
end
