require "rails_helper"

describe SchoolForm do
  describe "#save" do
    it "should return true" do
      form = SchoolForm.new(
        name: "APS High",
        street: "Any street",
        state: "NL",
        zip_code: "66230",
        country: "MX",
      )

      expect(form.save).to be_truthy
    end

    it "should return false if it is invalid" do
      form = SchoolForm.new(name: nil)

      expect(form.save).to be_falsey
    end
  end
end