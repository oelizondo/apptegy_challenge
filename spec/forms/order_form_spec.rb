require "rails_helper"

describe OrderForm do
  describe "#save" do
    it "should return true" do
      school = create :school
      form = OrderForm.new(send_email: false, gift_ids: [1,2], recipient_ids: [1,2], school: school)

      expect(form.save).to be_truthy
    end

    it "should return false if it is invalid" do
      school = create :school
      form = OrderForm.new(send_email: false, gift_ids: [1,2], recipient_ids: [1,2])

      expect(form.save).to be_falsey
    end
  end
end