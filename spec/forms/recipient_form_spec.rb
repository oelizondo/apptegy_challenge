require "rails_helper"

describe RecipientForm do
  describe "#save" do
    it "should return true" do
      school = create :school
      form = RecipientForm.new(
        first_name: "Oscar",
        last_name: "E.",
        street: "Any street",
        state: "NL",
        zip_code: "66230",
        country: "MX",
        school: school
      )

      expect(form.save).to be_truthy
    end

    it "should return false if it is invalid" do
      school = create :school
      form = RecipientForm.new(
        first_name: "Oscar",
        last_name: "E.",
        street: "Any street",
        state: "NL",
      )

      expect(form.save).to be_falsey
    end
  end
end