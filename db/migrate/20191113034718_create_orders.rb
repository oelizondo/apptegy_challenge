class CreateOrders < ActiveRecord::Migration[6.0]
  def change
    create_table :orders do |t|
      t.string :status, null: false, default: "order_processing"
      t.boolean :send_email, null: false, default: false
      t.references :school, null: false, foreign_key: true

      t.timestamps
    end
  end
end
