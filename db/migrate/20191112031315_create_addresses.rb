class CreateAddresses < ActiveRecord::Migration[6.0]
  def change
    create_table :addresses do |t|
      t.string :street, null: false
      t.string :zip_code, limit: 5, null: false
      t.string :country, null: false
      t.string :state, null: false
      t.bigint :owner_id, null: false
      t.string :owner_type, null: false

      t.timestamps
    end

    add_index :addresses, :owner_id
  end
end
