class CreateGiftOrders < ActiveRecord::Migration[6.0]
  def change
    create_table :gift_orders do |t|
      t.references :gift, null: false, foreign_key: true
      t.references :order, null: false, foreign_key: true

      t.timestamps
    end
  end
end
