# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
require "ffaker"

addresses = FactoryBot.create_list(:address, 5)

addresses.each do |address|
  FactoryBot.create :school, address: address
end

addresses = FactoryBot.create_list(:address, 5)
addresses.each do |address|
  FactoryBot.create :recipient, address: address
end

FactoryBot.create :gift, kind: "mug"
FactoryBot.create :gift, kind: "t_shirt"
FactoryBot.create :gift, kind: "hoodie"
FactoryBot.create :gift, kind: "sticker"

10.times do
  FactoryBot.create :order, recipients: Recipient.all.sample(3), gifts: Gift.all.sample(2)
  FactoryBot.create :user
end

