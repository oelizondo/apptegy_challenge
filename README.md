# Apptegy Coding Challenge

[![CircleCI](https://circleci.com/gh/oelizondo/apptegy_challenge.svg?style=svg&circle-token=440c880563275523b0e238e994567dba7cf2913f)](https://circleci.com/gh/oelizondo/apptegy_challenge)

## Installing

### Software Versions:

* Ruby: 2.6.2
* Rails: 6.0.1
* Postgres: 11.3

### Running

```
$ g clone https://github.com/oelizondo/apptegy_challenge.git
$ cd apptegy_challenge
$ bundle install
$ rails db:setup
$ rails db:create
$ rails s
```

## Initial Approach

I love building APIs and web services. My approach to building such interfaces is from the ground up. That is:

1. Think about the data model (tables).
2. Think about the relationships between those tables.
3. Think about the abstractions Rails provides for such tables.
  3.1. For example, Rails provides a lot of quality of like goodies like migrations and in-model associations like `belongs_to`, `has_many` and `has_many :x through: :y` that make thinking about models and their relationships very easy.
4. Thinking about how I want a user to interact with my API
  4.1. This basically means the endpoints I want to expose to the users.
5. Thinking about the usefulness of the data I return.
  5.1 For example a GET to a specific school should return only data that is relevant to the user. This is where `active_model_serializers` helped me to define the essential fields the JSON should return.
6. Write tests for models.
7. Write models to make tests pass.
8. Write serializers.
9. Write controllers.
10. Write forms.
11. Finish with authentication.

While this document starts off describing authentication and endpoints, it worth noting that my first approach was writing down my tables and their relationships with each other.

## Endpoints

Every endpoint is scoped under the namespace `/v1/`. The following resources were used to interact with the API:

* Users
* Schools
* Recipients
* Orders

The rest of the models do not need explicit endpoint. (Gifts and Addresses).

Keep in mind that to make POSTS to the API, you need to set your header to accept json:

```
Content-Type: application/json
```

### Users and JWT

All endpoints are protected and will return a `403 NOT AUTHORIZED` error if a valid JWT is not sent in the headers of the request. To generate a JWT you need to:

POST to `/v1/users/sign_up` with params:

```
{
  sign_up: {
    email: <some_email>,
    password: <some_password>,
    password_confirmation: <some_password>
  }
}
```
This will return a valid JWT that lasts 24 hours. The rest of the endpoints require that you set the following header:
```
Authorization: Bearer <your_token_here>
```

Implementation for encoding of the JWT is found in `lib` and looks like this:

```ruby
class JsonWebToken
  class << self
    def encode(payload, exp = 24.hours.from_now)
      payload[:exp] = exp.to_i
      JWT.encode(payload, Rails.application.secrets.secret_key_base)
    end

    def decode(token)
      body = JWT.decode(token, Rails.application.secrets.secret_key_base)[0]
      HashWithIndifferentAccess.new body
    rescue
      nil
    end
  end
end
```

The business logic for Authorizing and Authenticating a user is found in `app/services/auth`.

### Endpoints and params

These are the following endpoints:

#### Users

```
POST /v1/users/authenticate
POST /v1/users/sign_up
```

Params for generating a signing up:

```
{
  sign_up: {
    email: <some_email>,
    password: <some_password>,
    password_confirmation: <some_password>
  }
}
```

Params for authenticating as a user:

```
{
  user_auth: {
    email: <some_email>,
    password: <some_password>
  }
}
``` 

#### Schools

```
POST /v1/schools
GET /v1/schools
GET /v1/schools/:id
PATCH /v1/schools/:id
DELETE /v1/schools/:id
```

Params for generating a school:

```
{
  "school": {
    "name": "APS HIGH School"
  },
  "address": {
      "street": "Cerrada de Tecamachalco 1001",
      "zip_code": "66230",
      "country": "Mexico",
      "state": "NL",
      "county": "SPGG"
  }
}
```

#### Recipient

```
POST /v1/schools/:school_id/recipients
GET /v1/schools/:school/recipients
GET /v1/schools/:school/recipients/:id
PATCH /v1/schools/:school/recipients/:id
DELETE /v1/schools/:school/recipients/:id
```

Params for generating a recipient:

```
{
	"recipient": {
		"first_name": "oscar",
		"last_name": "elizondo"
	},
	
	"address": {
		"street": "CDA",
		"state": "NL",
		"country": "MX",
		"county": "SSGG",
		"zip_code": "66230"
	}
}
```

#### Orders

```
POST /v1/schools/:school_id/orders
GET /v1/schools/:school/orders
GET /v1/schools/:school/orders/:id
PATCH /v1/schools/:school/orders/:id
DELETE /v1/schools/:school/orders/:id
GET /v1/schools/:school/orders/:id/ship
GET /v1/schools/:school/orders/:id/cancel
GET /v1/schools/:school/orders/:id/received
```

> For the endpoint "received" I understood this was an ackowledgment from the recipients that they indeed received their gifts.

Params for generating an order:

```
{
	"order": {
		"send_email": "false",
		"gift_ids": [3,4],
		"recipient_ids": [1]
	}
}
```

## Data Model

From the project instructions there are five clear models that need to be in the app for it to work.

1. Schools
2. Addresses
3. Recipients
4. Order
5. Gifts

My thinking went along the lines of (and based on the document):

#### School

A school has an address.
A school has many recipients.
A school has many orders.

#### Addresses

An address belongs to an owner.
An owner can be either a school or a recipient.

This type of thinking lends itself to use a polymorphic association. Where the owner object can be either a school or a recipient.

#### Recipients

A recipient has an address.
A recipient belongs to a school.
A recipient has many shipments
A recipient has many orders through shipments.

A recipient has many orders, and an order has many recipients. I used a many to many relationship to fit this criteria, using the table shipments as the intermediary table.

#### Order

An order has many shipments.
An order has many recipients through shipments.
An order has many gift orders.
An order has many gifts.
An order belongs to a School.

Again, a many to many relationship was used because an order has many gifts and a gift has many orders associtated with it. Using the gift orders table as the intermediary.

#### Gift

A gift has many gift orders.
A gift has many orders.

Gifts are a special model, in the sense that they are not being created or destroyed. Only four instances of Gifts exist in the db. Each instance has a `kind`: mug, t_shirt, hoodie, sticker

### Other Models

I believe in the use of service object to keep controllers clean. FormObjects are a nice abstraction to creat several models in a single action. (Think an address has to be created together with a school). FormObjects behave similarly to regular Models, as they use `ActiveModel::Model` module to inherit behvior.

Example of a FormObject:

```ruby
class OrderForm
  include ActiveModel::Model

  attr_accessor :send_email, :gift_ids, :recipient_ids, :school
  attr_reader :order

  validates :send_email, presence: true
  validates :gift_ids, presence: true
  validates :recipient_ids, presence: true
  validates :school, presence: true

  def save
    if valid?
      process!
      true
    else
      false
    end
  end
end
```

As you can see, a FormObject has validations, a save method that return true or false in order to alert the controller what it must to incase the models is not saved.

In this case, an order is composed of its fields, gift_ids and recipient_ids. The `process!` method looks like this:

```ruby
  def process!
    @order ||= Order.create(
      send_email: send_email,
      gifts: gifts,
      recipients: recipients,
      school: school
    )
  end

  def gifts
    Gift.where(id: gift_ids)
  end

  def recipients
    Recipient.where(id: recipient_ids)
  end
```

The `forms` folder contains the following forms that comply to this same behavior.

* OrderForm
* SchoolForm
* RecipientForm

## Continous Integration and Deployment

I chose CircleCI as my test runner of choise. The reason being I am fairly familiar with the tool and that helped me progress fater. Right now every pull reques that is created runs the test and cannot be merged unless they pass.

I chose Heroku for deployment. The reason being it's the easiest and fastest way to get an API up and running. I like heroku because it takes care of the services an app needs like databases and other pugins you wish to include like a CDN (not applicable in this case). However, I recognize that for more complex applications another cloud provider like AWS or Digital Ocean are more powerful because of the tools they give developers, as well as complete control over the environment that an app is running in.

## Docker and Beyond.

I chose not to dockerize this project, but included a sample Dockerfile and docker-compose.yml file in the project that could work should we decide to implement it.

The reason I didn't dockerize the project is I didn't want to spend too much time on bonus points, and decided to focus on implementing a functional API before worrying about deployment.