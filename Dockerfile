FROM ruby:2.6.2-alpine AS runtime

WORKDIR /usr/src

ENV HOME=/usr/src PATH=/usr/src/bin:$PATH

EXPOSE 3000

CMD ["rails", "server", "-b", "0.0.0.0", "-p", "3000"]

RUN set -ex && apk add --no-cache \
  build-base \
  ca-certificates \
  less \
  libpq \
  openssl \
  postgresql-dev \
  tzdata

RUN apk add --update bash && rm -rf /var/cache/apk/*

ADD Gemfile* /usr/src/

USER nobody

RUN set -ex && bundle install --jobs=4 --retry=3