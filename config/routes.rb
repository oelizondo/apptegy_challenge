Rails.application.routes.draw do
  root to: "application#root"

  namespace :v1 do
    namespace 'users' do
      post 'authenticate', to: 'authentication#authenticate'
      post 'sign_up', to: 'registrations#create'
    end

    resources :schools do
      resources :recipients
      resources :orders
      get "orders/:id/ship", to: "orders#ship"
      get "orders/:id/cancel", to: "orders#cancel"
      get "orders/:id/received", to: "orders#received"
    end
  end
end
